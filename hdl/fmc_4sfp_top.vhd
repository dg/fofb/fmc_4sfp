library ieee;
use ieee.std_logic_1164.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity fmcbsp_4sfp is
    port(
        -- == == FMC SIDE == == --
        -- Transceivers clocks
        fmc_gbtclk0_p                              : in std_logic;
        fmc_gbtclk0_n                              : in std_logic;

        -- LA pins, connect all but uses a few
        fmc_la_p                                   : inout std_logic_vector(33 downto 0);
        fmc_la_n                                   : inout std_logic_vector(33 downto 0);

        -- MGT differential pairs, only fours used
        fmc_dp_m2c_p                               : in std_logic_vector(3 downto 0);
        fmc_dp_m2c_n                               : in std_logic_vector(3 downto 0);
        fmc_dp_c2m_p                               : out std_logic_vector(3 downto 0);
        fmc_dp_c2m_n                               : out std_logic_vector(3 downto 0);

        -- == == LOGIC SIDE == == --
        osc_i2c_scl                                : in std_logic;
        osc_i2c_sda                                : inout std_logic;
        osc_ref_clk_p                              : out std_logic;
        osc_ref_clk_n                              : out std_logic;
        sfp_tx_n                                   : in std_logic_vector(3 downto 0);
        sfp_tx_p                                   : in std_logic_vector(3 downto 0);
        sfp_rx_n                                   : out std_logic_vector(3 downto 0);
        sfp_rx_p                                   : out std_logic_vector(3 downto 0);
        sfp_rxlos                                  : out std_logic_vector(3 downto 0);
        sfp_mod_abs                                : out std_logic_vector(3 downto 0);
        sfp_tx_disable                             : in std_logic_vector(3 downto 0);
        sfp_tx_fault                               : out std_logic_vector(3 downto 0);
        sfp_rs0                                    : in std_logic_vector(3 downto 0);
        sfp_rs1                                    : in std_logic_vector(3 downto 0);
        sfp_i2c_scl                                : in std_logic_vector(3 downto 0);
        sfp_i2c_sda                                : inout std_logic_vector(3 downto 0)
    );
end entity fmcbsp_4sfp;

architecture struct of fmcbsp_4sfp is

begin

    osc_ref_clk_p <= fmc_gbtclk0_p;
    osc_ref_clk_n <= fmc_gbtclk0_n;

    fmc_dp_c2m_n <= sfp_tx_n;
    fmc_dp_c2m_p <= sfp_tx_p;
    sfp_rx_n     <= fmc_dp_m2c_n;
    sfp_rx_p     <= fmc_dp_m2c_p;

    inst_buf_mod_abs_0:IOBUF
    port map(IO=>fmc_la_n(6), O=>sfp_mod_abs(0), I=>'0', T=>'1');
    inst_buf_mod_abs_1:IOBUF
    port map(IO=>fmc_la_p(7), O=>sfp_mod_abs(1), I=>'0', T=>'1');
    inst_buf_mod_abs_2:IOBUF
    port map(IO=>fmc_la_p(6), O=>sfp_mod_abs(2), I=>'0', T=>'1');
    inst_buf_mod_abs_3:IOBUF
    port map(IO=>fmc_la_n(7), O=>sfp_mod_abs(3), I=>'0', T=>'1');

    inst_buf_rxlos_0:IOBUF
    port map(IO=>fmc_la_n(4), O=>sfp_rxlos(0), I=>'0', T=>'1');
    inst_buf_rxlos_1:IOBUF
    port map(IO=>fmc_la_p(5), O=>sfp_rxlos(1), I=>'0', T=>'1');
    inst_buf_rxlos_2:IOBUF
    port map(IO=>fmc_la_p(4), O=>sfp_rxlos(2), I=>'0', T=>'1');
    inst_buf_rxlos_3:IOBUF
    port map(IO=>fmc_la_n(5), O=>sfp_rxlos(3), I=>'0', T=>'1');

    inst_buf_tx_fault_0:IOBUF
    port map(IO=>fmc_la_n(2), O=>sfp_tx_fault(0), I=>'0', T=>'1');
    inst_buf_tx_fault_1:IOBUF
    port map(IO=>fmc_la_p(3), O=>sfp_tx_fault(1), I=>'0', T=>'1');
    inst_buf_tx_fault_2:IOBUF
    port map(IO=>fmc_la_p(2), O=>sfp_tx_fault(2), I=>'0', T=>'1');
    inst_buf_tx_fault_3:IOBUF
    port map(IO=>fmc_la_n(3), O=>sfp_tx_fault(3), I=>'0', T=>'1');

    inst_buf_tx_disable_0:IOBUF
    port map(IO=>fmc_la_n(8), I=>sfp_tx_disable(0), O=>open, T=>'0');
    inst_buf_tx_disable_1:IOBUF
    port map(IO=>fmc_la_p(9), I=>sfp_tx_disable(1), O=>open, T=>'0');
    inst_buf_tx_disable_2:IOBUF
    port map(IO=>fmc_la_p(8), I=>sfp_tx_disable(2), O=>open, T=>'0');
    inst_buf_tx_disable_3:IOBUF
    port map(IO=>fmc_la_n(9), I=>sfp_tx_disable(3), O=>open, T=>'0');

    inst_buf_rs0_0:IOBUF
    port map(IO=>fmc_la_n(10), I=>sfp_rs0(0), O=>open, T=>'0');
    inst_buf_rs0_1:IOBUF
    port map(IO=>fmc_la_p(11), I=>sfp_rs0(1), O=>open, T=>'0');
    inst_buf_rs0_2:IOBUF
    port map(IO=>fmc_la_p(10), I=>sfp_rs0(2), O=>open, T=>'0');
    inst_buf_rs0_3:IOBUF
    port map(IO=>fmc_la_n(11), I=>sfp_rs0(3), O=>open, T=>'0');

    inst_buf_rs1_0:IOBUF
    port map(IO=>fmc_la_n(12), I=>sfp_rs1(0), O=>open, T=>'0');
    inst_buf_rs1_1:IOBUF
    port map(IO=>fmc_la_p(13), I=>sfp_rs1(1), O=>open, T=>'0');
    inst_buf_rs1_2:IOBUF
    port map(IO=>fmc_la_p(12), I=>sfp_rs1(2), O=>open, T=>'0');
    inst_buf_rs1_3:IOBUF
    port map(IO=>fmc_la_n(13), I=>sfp_rs1(3), O=>open, T=>'0');

    inst_buf_i2c_scl_0:IOBUF
    port map(IO=>fmc_la_p(24), I=>sfp_i2c_scl(0), O=>open, T=>'0');
    inst_buf_i2c_scl_1:IOBUF
    port map(IO=>fmc_la_p(22), I=>sfp_i2c_scl(1), O=>open, T=>'0');
    inst_buf_i2c_scl_2:IOBUF
    port map(IO=>fmc_la_p(23), I=>sfp_i2c_scl(2), O=>open, T=>'0');
    inst_buf_i2c_scl_3:IOBUF
    port map(IO=>fmc_la_p(21), I=>sfp_i2c_scl(3), O=>open, T=>'0');

    sfp_i2c_sda(0) <= fmc_la_n(24);
    sfp_i2c_sda(1) <= fmc_la_n(22);
    sfp_i2c_sda(2) <= fmc_la_n(23);
    sfp_i2c_sda(3) <= fmc_la_n(21);


    inst_buf_osc_i2c_scl:IOBUF
    port map(IO=>fmc_la_n(25), I=>osc_i2c_scl, O=>open, T=>'0');

    osc_i2c_sda <= fmc_la_p(25);

end architecture struct;








